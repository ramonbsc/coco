from extasy import script

import MDAnalysis as mda
import numpy as np
import traceback

#
# All simulations use the same mdin file.
#
dict = {}
dict['crdfile'] = 'penta.crd'
dict['topfile'] = 'penta.top'
dict['mdin']    = 'mdshort.in'
dict['minin']   = 'min.in'

# set number of cycles, and number of replicates
maxcycles = 10
nreps = 8

#
# Main loop begins here:
#
for cycle in range(maxcycles):
    dict['cycle'] = str(cycle)
    if cycle == 0:
#
# Cycle 0: We begin with a single coordinate and topology file. 
# Copy it into the separate rep0? directories ready to run the MD simulations.
#
        cp = script.Script()
        for rep in range(nreps):
            dict['rep'] = str(rep)
            cp.append('cp {crdfile} rep0{rep}/min0.crd'.format(**dict))
        try:
            cp.run('tcsh -f {}')
        except:
            print 'Error copying input file into replicate directories'
            traceback.print_exc()
            exit(1)
    else:        
        print 'coco stuff...'
        
        cs = script.Script()
        cs.append('pyCoCo --grid 30 --dims 3  --frontpoints 8 --topfile {topfile} --mdfile rep0?/*.ncdf --output pentaopt{cycle} -vvv'.format(**dict))
#        cs.append('mpiexec -mca btl ^openib -n 4 python cocoUi.py --grid 30 --dims 3  --frontpoints 8 --topfile {topfile} --mdfile rep0?/*.mdcrd --output pentaopt{cycle} -vvv'.format(**dict))

        try:        
        	cs.run('tcsh -f {}')
        except:
        	print "CoCo run failed."
        	traceback.print_exc()
        	exit(1)
		

# Create new starting structures by running the optimised pdb files through tleap.

        print 'creating new crd files...'
        for rep in range(nreps):
            dict['rep'] = rep
            tl = script.Script()
            tl.append('source leaprc.ff99SB')
            tl.append('x = loadpdb pentaopt{cycle}{rep}.pdb'.format(**dict))
            tl.append('saveamberparm x rep0{rep}/delete.me rep0{rep}/min{cycle}.crd'.format(**dict))
            tl.append('quit')
            try:
                tl.run('tleap -f {}')
            except:
                'Error running leap'
                traceback.print_exc()
#
# Now run the MD.
#
    md0 = script.Script()
    for i in range(nreps):
        dict['rep'] = str(i)
        # output files that need to be transferred back: *.mdcrd
        md0.append('pmemd -O -i {minin} -o rep0{rep}/min{cycle}.out -inf rep0{rep}/min{cycle}.inf -r rep0{rep}/md{cycle}.crd -p {topfile} -c rep0{rep}/min{cycle}.crd -ref rep0{rep}/min{cycle}.crd'.format(**dict))
        md0.append('pmemd -O -i {mdin} -o rep0{rep}/md{cycle}.out -inf rep0{rep}/md{cycle}.inf -x rep0{rep}/md{cycle}.ncdf -r rep0{rep}/md{cycle}.rst -p {topfile} -c rep0{rep}/md{cycle}.crd'.format(**dict))

    print 'running md cycle {cycle}'.format(**dict)
    try:
        out = md0.run("tcsh -f {}")
    except:
        print 'Error running pmemd jobs'
        traceback.print_exc()
        exit(1)

