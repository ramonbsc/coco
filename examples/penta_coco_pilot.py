import os

from extasy import cofasu
from extasy import pcz
from extasy import optimizer
from extasy import coco
from extasy import script

import MDAnalysis as mda
import numpy as np

import radical.pilot as rp

DBURL = "mongodb://ec2-184-72-89-141.compute-1.amazonaws.com:27017"
RCONF  = ["https://raw.github.com/radical-cybertools/radical.pilot/master/configs/xsede.json"]

#------------------------------------------------------------------------------
#
def pilot_state_cb(pilot, state):
    """pilot_state_change_cb() is a callback function. It gets called very
    time a ComputePilot changes its state.
    """
    print "[Callback]: ComputePilot '{0}' state changed to {1}.".format(
        pilot.uid, state)

    if state == rp.states.FAILED:
        sys.exit(1)

#------------------------------------------------------------------------------
#
def unit_state_change_cb(unit, state):
    """unit_state_change_cb() is a callback function. It gets called very
    time a ComputeUnit changes its state.
    """
    print "[Callback]: ComputeUnit '{0}' state changed to {1}.".format(
        unit.uid, state)
    if state == rp.states.FAILED:
        print "            Log: %s" % unit.log[-1]


#------------------------------------------------------------------------------
#
print "Starting 16 core pilot-job..."
session = rp.Session(database_url=DBURL)
pmgr = rp.PilotManager(session=session, resource_configurations=RCONF)
pmgr.register_callback(pilot_state_cb)

pdesc = rp.ComputePilotDescription()
pdesc.resource  = "stampede.tacc.utexas.edu-local"
pdesc.runtime   = 60 # minutes
pdesc.cores     = 16    
pdesc.cleanup   = False
pilot = pmgr.submit_pilots(pdesc)

umgr = rp.UnitManager(session=session, scheduler=rp.SCHED_DIRECT_SUBMISSION)
umgr.register_callback(unit_state_change_cb)
umgr.add_pilots(pilot)
print "... DONE."

#------------------------------------------------------------------------------
#

#
# All simulations use the same mdin file.
#
dict = {}
dict['crdfile'] = 'penta.crd'
dict['topfile'] = 'penta.top'
dict['mdin']    = 'mdshort.in'
dict['minin']   = 'min.in'

# set number of cycles, and number of replicates
maxcycles = 10
nreps = 8

#
# Main loop begins here:
#
for cycle in range(maxcycles):
    dict['cycle'] = str(cycle)
    if cycle == 0:
#
# cycle 0: we begin with a single coordinate and topology file. 
# Copy it into the separate rep0? directories ready to run the MD simulations.
#
        print "creating initial setup"
        cp = script.Script()
        for rep in range(nreps):
            dict['rep'] = str(rep)
            dict['path'] = os.getcwd()
            cp.append('cp {crdfile} {path}/rep0{rep}/md0.crd'.format(**dict))
        try:
            cp.run('tcsh -f {}')
        except:
            print 'Error copying input file into replicate directories'
            exit(1)
    else:
#
# generate new start structures through COCO analysis of all trajectory data
# so far
#
        print 'running CoCo...'
        cs = script.Script()
        cs.append('python pycoco.py --grid 10 --dims 3  --frontpoints 8 --topfile {topfile} --mdfile rep0?/*.ncdf --output pentaopt{cycle} -vvv'.format(**dict))
        try:
                cs.run('tcsh -f {}')
        except:
                print "CoCo run failed."
                exit(1)

# Create new starting structures by running the optimised pdb files through tleap.

        print 'creating new crd files...'
        for rep in range(nreps):
            dict['rep'] = rep
            tl = script.Script()
            tl.append('source leaprc.ff99SB')
            tl.append('x = loadpdb pentaopt{cycle}{rep}.pdb'.format(**dict))
            tl.append('saveamberparm x rep0{rep}/delete.me rep0{rep}/min{cycle}.crd'.format(**dict))
            tl.append('quit')
            try:
                tl.run('tleap -f {}')
            except:
                'Error running leap'
                exit(1)
#
# now run the MD
#
    compute_units = []
    for i in range(nreps):
        print "Submitting new 'pmemd' compute unit"
        dict['rep'] = str(i)
        dict['path'] = os.getcwd()
        # output files that need to be transferred back: *.mdcrd

        step_1 = 'pmemd -O -i {path}/{minin} -o {path}/rep0{rep}/min{cycle}.out -inf {path}/rep0{rep}/min{cycle}.inf -r {path}/rep0{rep}/md{cycle}.crd -p {path}/{topfile} -c {path}/rep0{rep}/min{cycle}.crd -ref {path}/rep0{rep}/min{cycle}.crd'.format(**dict)
        step_2 = 'pmemd -O -i {path}/{mdin} -o {path}/rep0{rep}/md{cycle}.out -inf {path}/rep0{rep}/md{cycle}.inf -x {path}/rep0{rep}/md{cycle}.ncdf -r {path}/rep0{rep}/md{cycle}.rst -p {path}/{topfile} -c {path}/rep0{rep}/md{cycle}.crd'.format(**dict)

        cu = rp.ComputeUnitDescription()
        cu.executable = "/bin/bash"
        cu.cores      = 1
        cu.arguments  = ['-l', '-c', '"module load TACC && module load amber && %s && %s"' % (step_1, step_2)]
        compute_units.append(cu)

    units = umgr.submit_units(compute_units)
    umgr.wait_units()

    #print 'running md cycle {cycle}'.format(**dict)
    #try:
    #    out = md0.run("tcsh -f {}")
    #except IOError as e:
    #    print "I/O error({0}): {1}".format(e.errno, e.strerror)

session.close()

