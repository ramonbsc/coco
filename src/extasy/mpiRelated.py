# EPCC
'''
This routine looks after the MPI-parallelization bits.
It takes care of the case that mpi4py is not available.
'''
import numpy as np

try:
    from mpi4py import MPI
    parallel = True
except ImportError:
    parallel = False

if parallel:
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()
else:
    comm = None
    rank = 0
    size = 1

