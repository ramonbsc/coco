#!/usr/bin/python
'''
The pcz class. 
'''
from MDAnalysis.analysis.align import *
import numpy as np
from scipy.linalg import *
import struct

class Pcz:
   def __init__(self, cofasu, target=None, quality=90.0, log=None,
   req_evecs=None):
        '''
        Initialises a new pcz object with the data from the given
        cofasu object. 'Target' can be a precalculated global average
        structure. The quality setting defaults to 90%. 'log'
        provides a hook for a python logger instance.
        '''
        self.cofasu = cofasu 
        if log is not None:
            log.info('Pcz: {} atoms and {} snapshots'.format(cofasu.natoms, cofasu.numframes()))
        if log is not None:
            log.info('Pcz: least-squares fitting snapshots')
        if target is None:
            self._avg = cofasu.fitted_average()
        else:
            self._avg = target

        self.quality = quality
        self.natoms = self.cofasu.natoms
        self.nframes = self.cofasu.numframes()

        trj = np.zeros((self.nframes,self.natoms*3))
        for i in range(self.nframes):
            mob = self.cofasu.coords(i)
            mob -= mob.mean(axis=0)
            R, rms = rotation_matrix(self._avg,mob)
            trj[i,:] = np.dot(mob,R).flatten()-self._avg.flatten()

        if log is not None:
            log.info('Pcz: calculating covariance matrix')
#        cv = np.cov(trj,rowvar=0)
        cv = self.cofasu.cov(self._avg)
        if log is not None:
            log.info('Pcz: diagonalizing covariance matrix')
        w,v = eigh(cv)

        cs = np.cumsum(w[::-1])
        self.totvar = cs[-1]
        tval = cs[-1]*self.quality/100
        i=0
        while cs[i] < tval:
            i += 1

        i += 1
        self.nvecs =  i
        self._evals = w[-1:-(i+1):-1]
        self._evecs = v[:,-1:-(i+1):-1].T
        if log is not None:
            log.info('Pcz: calculating projections')
        self._projs = np.dot(trj,self._evecs.T).T

   def avg(self):
      """ 
      Returns the average structure contained in the pcz file
      as an (natoms,3) numpy array.
      """
      return self._avg

   def eval(self,ival):
      """ 
      Returns an eigenvalue from the file.
      """
      if ival >= self.nvecs:
         print 'Error - only ',self.nvecs,' eigenvectors present'
         return 0.0
      else:
         return self._evals[ival]

   def evals(self):
      """
      Returns an array of all eigenvalues in the file.
      """
      return self._evals

   def evec(self,ivec):
      """
      Returns a chosen eigenvector from the file in the
      form of a (3*natoms) numpy array.
      """
      if ivec >= self.nvecs:
         print 'Error - only ',self.nvecs, 'eigenvectors present'
         return None
      else:
         return self._evecs[ivec,:]

   def evecs(self):
      """
      Returns all eigenvectors in the file in the form of a
      (nvecs,3*natoms) numpy array.
      """
      return self._evecs

   def proj(self,iproj):
      """
      Returns an array of the projections along a given eigenvector. There
      will be one value per snapshot.
      """
      if iproj >= self.nvecs:
         print 'Error - only ',self.nvecs, 'eigenvectors present'
         return None
      else:
         return self._projs[iproj,:]

   def scores(self, framenumber):
      """
      Method that returns the scores (projections) corresponding to
      a chosen snapshot.
      """
      if( framenumber > self.nframes):
           return None
      else:
           x = np.zeros(self.nvecs)
           for i in range(self.nvecs):
               x[i] = self.proj(i)[framenumber-1]
           return x

   def frame(self,framenumber):
      """
      Method to return the coordinates of the given frame - i.e.
      to decompress a snapshot. The data is returned as a (natoms,3) 
      numpy array.
      """
      if(framenumber > self.nframes):
          return None
      else:
          scores = self.scores(framenumber)
          return self.unmap(scores)

   def unmap(self,scores):
      """
      Method to return the coordinates corresponding to a given
      set of scores. If the scores vector has less than nvec elements,
      Pczfile.expand is called to fill in the missing values.
      """
      x = self.avg()
      if len(scores) < self.nvecs:
          scores = self.expand(scores)
      for i in range(self.nvecs):
          x = x + (self.evec(i)*scores[i]).reshape((self.natoms,3))
      return x

   def expand(self, scores):
      """
      Method to complete a truncated list of scores with values for
      missing eigenvectors. Basically the pcz file is scanned to
      find the frame with the best match to the scores given, the
      missing values are then copied directly from this frame.
      """
      nvecs = self.nvecs
      ns = len(scores)
      if ns >= nvecs:
          return scores
      else:
          newscores = np.zeros(nvecs)
          newscores[0:ns] = scores
          temp = self._projs
          best = 0
          err = ((temp[0:ns,0]-scores)*(temp[0:ns,0]-scores)).sum()
          newscores[ns:nvecs] = temp[ns:nvecs,0]
          for frame in range(self.nframes):
              newerr = ((temp[0:ns,frame]-scores)*(temp[0:ns,frame]-scores)).sum()
              if newerr < err:
                  err = newerr
                  newscores[ns:nvecs] = temp[ns:nvecs,frame]
          return newscores

   def map(self,crds):
      """
      Method to map an arbitrary coordinate set onto the PC model. The
      coordinate set should be a (natom,3) array-like object that matches
      (for size) what's in the pczfile. An array of projections will be 
      returned, one value for each eignevector in the pcz file.
      """
      crdset=np.array(crds)
      if np.shape(crdset) == (self.natoms, 3):
        avg = np.reshape(self.avg(),(self.natoms,3))
        rot = np.zeros(9, dtype=np.float64)
        crdset_cog = crdset.sum(axis=0)/crdset.shape[0]
        avg_cog = avg.sum(axis=0)/avg.shape[0]
        c = crdset.copy()
        c -= crdset_cog
        rmsd = qcp.CalcRMSDRotationalMatrix((avg-avg_cog).T.astype(np.float64),
                                  c.T.astype(np.float64),
                                  crdset.shape[0], rot, None)
        R = np.matrix(rot.reshape(3,3))
        c = c * R
        c += avg_cog
        c -= avg
        prj = np.zeros(self.nvecs)
        for i in range(self.nvecs):
            prj[i]=(np.dot(c.flatten(),self.evec(i)))
        return prj
      else:
        return None

   def write(self,filename, version='PCZ4', title='Created by pcz.write()'):
       '''
       Write out the PCZ file. At the moment only the PCZ4 format is implemented.
       '''
       if version != 'PCZ4':
           raise TypeError('Only PCZ4 format supported')

       f = open(filename, 'wb')
       f.write(struct.pack('4s80s3if', version,title,self.natoms,self.nframes,self.nvecs,self.totvar)) 
       f.write(struct.pack('4i',0.0,0.0,0.0,0.0))
       for v in self.avg().flatten():
           f.write(struct.pack('f',v))
       for i in range (self.nvecs):
           for v in self.evec(i): 
               f.write(struct.pack('f',v))
           f.write(struct.pack('f',self.eval(i)))
           for v in self.proj(i):
               f.write(struct.pack('f',v))
           
       f.close()
       return
