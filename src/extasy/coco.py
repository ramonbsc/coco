# Python program that implements coco in Python.
#
# @Copyright
# Dr. Ardita Shkurti
# Dr. Charles Laughton
# The University of Nottingham
# Contact: ardita.shkurti@gmail.com

import numpy as np
import scipy.ndimage as nd

class Coco():
    
    def __init__(self, projs, resolution=10):
        '''
        Initialise a COCO map. This is essentially a multidimensional histogram
        produced from a set of coordinate data, projs(N,D) where N is the
        number of points and D is the number of dimensions. "resolution" 
        specifies the number of bins in each dimension, and may be a single 
        number or a list of length D.
        '''
        self.ndim = projs.shape[1]
        # resolution may be a single value or a list of values, 1 per dimension
        self.resolution = np.zeros(self.ndim)
        self.resolution[:] = resolution
        self.range = []
        min = projs.min(axis=0)
        max = projs.max(axis=0)
        # set a buffer, so that there is a clear 1-bin boundary around the map.
        # This is for future "halo" and "frontier" point-choosing methods.
        for i in range(self.ndim):
            buff = (max[i]-min[i])/(self.resolution[i]-2)*1.01
            self.range.append((min[i]-buff,max[i]+buff))
        
        # Create the histogram
        self.H, self.edges = np.histogramdd(projs, bins=self.resolution,
        range=self.range)
        # find the pixel width values (this should be buff, but don't assume)
        self.ginc = []
        for i in range(self.ndim):
            self.ginc.append(self.edges[i][1]-self.edges[i][0])
        
    def cpoints(self,npoints):
        '''
        cpoints(np) returns new points, generated using the COCO procedure,
        in the form of an (npoints,D) numpy array, where D is the number of
        dimensions in the map.
        '''
        cp = np.zeros((npoints,self.ndim))
        # make a temporary binary image, and invert
        tmpimg = np.where(self.H > 0, 0, 1)
        for i in range(npoints):
            self.dis = nd.morphology.distance_transform_edt(tmpimg)
            indMax = np.unravel_index(self.dis.argmax(),self.dis.shape)
            for j in range(self.ndim):
                cp[i,j]=self.edges[j][0]+indMax[j]*self.ginc[j]
            
            tmpimg[indMax] = 0
        return cp
