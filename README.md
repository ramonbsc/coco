# What is CoCo?

CoCo ("Complementary Coordinates") is a method for testing and potentially enriching the the variety of conformations within an ensemble of molecular structures. It was originally developed with NMR datasets in mind and the background and this application is described in:

[Laughton C.A., Orozco M. and Vranken W., COCO: A simple tool to enrich the representation of conformational variability in NMR structures, PROTEINS, 75, 206-216 (2009)](http://onlinelibrary.wiley.com/doi/10.1002/prot.22235/abstract)

CoCo, which is based on principal component analysis, analyses the distribution of an ensemble of structures in conformational space, and generates a new ensemble that fills gaps in the distribution. These new structures are not guaranteed to be valid members of the ensemble, but should be treated as possible, approximate, new solutions for refinement against the original data.
Though developed with protein NMR data in mind, the method is quite general – the initial structures do not have to come from NMR data, and can be of nucleic acids, carbohydrates, etc.

The outline of the CoCo method is as follows:

![COCO method2.gif](https://bitbucket.org/repo/bAGR4M/images/1692328909-COCO%20method2.gif)
Step 1: The input ensemble is subjected to Principal Component Analysis and the position of each structure in a low dimensional subspace identified.

Step 2: New points are placed to fill gaps in the distribution.

Step 3: The new points are converted back into new structures, which are returned to the user.


This package provides a python-based command line tool that implements the CoCo method, and also exemplifies its application within an ExTASY workflow that can be used in an iterative manner to rapidly expand the coverage of a MD-generated ensemble.

------

# Installation

## ... on Ubuntu Linux (Desktop)

The following packages need to be installed on the system (via `apt-get`) `python-dev, libblas-dev, liblapack-dev, gfortran, tcsh`, as well as `Amber`.

Having `numpy` and `scipy` is a prerequisite to the CoCo installation as well.

After making sure that the previously listed dependencies are installed, clone the CoCo repository and install:

```
git clone https://bitbucket.org/extasy-project/coco.git
cd coco
easy_install --upgrade --user .
chmod +x $HOME/.local/lib/python2.7/site-packages/radical.pilot-0.19-py2.7.egg/radical/pilot/bootstrapper/*sh
```

## ... on XSEDE's Stampede Cluster

First, we need to load the appropriate modules:

```
module load amber
module load python/2.7.3-epd-7.3.2
```

Clone the CoCo repository and install:

```
rm -r $HOME/.local/*
git clone https://bitbucket.org/extasy-project/coco.git
cd coco
easy_install --user --upgrade .
chmod +x $HOME/.local/lib/python2.7/site-packages/radical.pilot-0.13-py2.7.egg/radical/pilot/bootstrapper/*.sh
```
Ensure the executables are in your PATH:
```
export PATH=$PATH:$HOME/.local/bin
```

## ... on ARCHER

First, we need to load the appropriate modules:

```
module load amber
module load numpy/1.8.0-libsci
module load scipy
```

Clone the CoCo repository and install (Must be on /work to run on the backend):

```
export WORK=/path/to/your/work/directory
rm -r $WORK/.local
git clone https://bitbucket.org/extasy-project/coco.git
cd coco
mkdir -p $WORK/.local/lib/python2.7/site-packages
export PYTHONPATH=$PYTHONPATH:$WORK/.local/lib/python2.7/site-packages
easy_install --prefix $WORK/.local --upgrade .
```

# Running the Workflow Example
The examples directory illustrates how pyCoCo can be used as part of an ExTASY workflow, running either on a local machine or a remote HPC resource, to perform enhanced sampling. In this case, we begin by running eight independent short MD simulations on alanine pentapeptide. The combined trajectory data is fed into a CoCo analysis to identify poorly sampled regions and generate eight new starting structures that populate these. These structures are then used in a new cycle of eight MD simulations, the new trajectory data is combined with the old data for a second CoCo analysis, etc. In this case ten cycles of MD/CoCo are run.
(CoCo workflow diagram here)

## Sequentially / Locally 

Change into the examples directory:
```
cd examples
```

Create the directories for the replicas (**delete them if they already exist**):

```
rm -r rep*
mkdir rep00 rep01 rep02 rep03 rep04 rep05 rep06 rep07
```

Then run the example:

```
python penta_coco.py
```

The output should look like this:

```
running md cycle 0
coco stuff...
creating new crd files...
running md cycle 1
[...]
```

## Concurrently via RADICAL-Pilot on Stampede

This example is very similar to the example above. The only difference is that 
it uses RADICAL-Pilot () to launch the AMBER jobs via a pilot job that is 
submitted to the local queueing system before the simluations are started. 
The pilot job stays alive throughout all cycles and executes all 8 amber 
tasks per cycle concurrently. 

Use the **Install on XSEDE's Stampede Cluster** instructions from above.

Change into the examples directory:

```
cd examples
```

Create the directories for the replicas (**delete them if they already exist**):

```
rm -r rep*
mkdir rep00 rep01 rep02 rep03 rep04 rep05 rep06 rep07
```

Then run the example:

```
python penta_coco_pilot.py
```

The output should look like this:

```
Starting 16 core pilot-job...
... DONE.
creating initial setup
[Callback]: ComputePilot '537acd9941a63348f8a01e2b' state changed to Launching.
Submitting new 'pmemd' compute unit
Submitting new 'pmemd' compute unit
Submitting new 'pmemd' compute unit
Submitting new 'pmemd' compute unit
Submitting new 'pmemd' compute unit
Submitting new 'pmemd' compute unit
Submitting new 'pmemd' compute unit
Submitting new 'pmemd' compute unit
[Callback]: ComputeUnit '537acd9941a63348f8a01e2d' state changed to PendingExecution.
[Callback]: ComputeUnit '537acd9941a63348f8a01e2e' state changed to PendingExecution.
[Callback]: ComputeUnit '537acd9941a63348f8a01e2f' state changed to PendingExecution.
[Callback]: ComputeUnit '537acd9941a63348f8a01e30' state changed to PendingExecution.
[...]
creating cofasu...
cofasu contains  59  and  8000  frames
running pcazip...
coco stuff...
creating new crd files...
Submitting new 'pmemd' compute unit
Submitting new 'pmemd' compute unit
Submitting new 'pmemd' compute unit
[...]
```

# Using the Command Line Tool "pyCoCo"

## A simple example

Given an ensemble of structures in the form of a trajectory file _ensemble.trj_, compatible with the topology file _protein.top_, we can generate four new 'complementary' coordinate sets, in the form of four PDB format files named _new0.pdb_, _new1.pdb_,..,_new3.pdb_, as follows:

```
pyCoCo -i ensemble.trj -t protein.top -o new -n 4 -vv
```
The output should look like this:
```
INFO: Verbose output.
INFO: creating cofasu...
DEBUG: Universe.load_new(): loading ensemble.trj...
INFO: cofasu contains 59 atoms and 100 frames
INFO: creating optimizer...
INFO: running pcazip...
INFO: generating new points...
```

pyCoCo accepts most of the most widely-used trajectory and topology file formats (AMBER, CHARMM, GROMACS, NAMD, etc.). For a full guide to pyCoCo see [here](http://bitbucket.org/extasy-project/coco/wiki/Home).

# Developing Coco

You can directly work with the source files in the `src/extasy/` directory. However, in order for the changes to take effect, you need to run the installer before you run your test / example code:

```
easy_install --upgrade .
```

For example, if you are in the examples directory and you want to see if your latest code changes to, let's say `src/extasy/coco.py` work, you can simply run:

```
easy_install --upgrade .. && python penta_coco.py