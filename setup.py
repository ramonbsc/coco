""" Setup script. Used by easy_install and pip. """

import os
import sys
import subprocess as sp

from setuptools import setup, find_packages, Command


#-----------------------------------------------------------------------------
# check python version. we need > 2.5, <3.x
if  sys.hexversion < 0x02050000 or sys.hexversion >= 0x03000000:
    raise RuntimeError("%s requires Python 2.x (2.5 or higher)" % name)


#-----------------------------------------------------------------------------
#
def read(*rnames):
    return open(os.path.join(os.path.dirname(__file__), *rnames)).read()


#-----------------------------------------------------------------------------
setup_args = {
    'name'             : "extasy.coco",
    'version'          : "0.1",
    'description'      : "EXTASY Project - Coco",
    'long_description' : (read('README.md') + '\n\n' + read('CHANGES.md')),
    'author'           : "The EXTASY Project",
    'url'              : "https://bitbucket.org/extasy-project/extasy-project",
    'license'          : "T.B.D",
    'classifiers'      : [
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'Environment :: Console',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.5',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Topic :: Utilities',
        'Topic :: System :: Distributed Computing',
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: POSIX',
        'Operating System :: Unix'
    ],

    'namespace_packages': ['extasy'],
    'packages'    : find_packages('src'),
    'package_dir' : {'': 'src'},
    'scripts' : ['examples/pyCoCo'],
    'install_requires' : ['numpy',
                          'scipy',
    		          'MDAnalysis',
                          'radical.pilot'],
    'zip_safe'         : False,
}

#-----------------------------------------------------------------------------

setup (**setup_args)
